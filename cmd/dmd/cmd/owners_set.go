package cmd

import (
	"database/sql"

	"dmd.tanna.dev/internal/ownership"
	"github.com/spf13/cobra"
)

var ownersSetCmd = &cobra.Command{
	Use:   "set owner [notes]",
	Short: "Set the ownership of repo(s) in bulk",
	Long: `Set the ownership of repo(s) in bulk, by using flags to filter

i.e.

dmd owners set 'Jamie Tanna' --organisation jamietanna
dmd owners set --platform 'github' --organisation jenkinsci --repo job-dsl-plugin 'Job DSL Plugin developers' 'https://github.com/orgs/jenkinsci/teams/job-dsl-plugin-developers'
`,
	Args: cobra.RangeArgs(1, 2),
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		params := ownership.SetBulkOwnershipParams{
			Platform:     platform,
			Organisation: organisation,
			Repo:         repo,
			OwnerName:    args[0],
		}

		if len(args) > 1 {
			params.Notes = args[1]
		}

		err = ownership.SetBulkOwnership(cmd.Context(), params, sqlDB)
		cobra.CheckErr(err)
	},
}

func init() {
	ownersCmd.AddCommand(ownersSetCmd)
	ownersSetCmd.Flags().StringVar(&platform, "platform", "", "")
	ownersSetCmd.Flags().StringVar(&organisation, "organisation", "", "")
	ownersSetCmd.Flags().StringVar(&repo, "repo", "", "")
}
