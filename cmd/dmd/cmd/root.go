package cmd

import (
	"fmt"
	"os"

	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
)

var (
	databasePath string
	outPath      string
	debug        bool
	noProgress   bool
	platform     string
	organisation string
	repo         string
	csvOutput    bool

	logger log.Logger

	versionInfo struct {
		version string
		commit  string
		short   string
	}
)

var rootCmd = &cobra.Command{
	Use:   "dmd",
	Short: "A set of tooling to interact with dependency-management-data",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if debug {
			logger = log.New(log.WithLevel(log.DebugLevel))
			logger.Debug("Starting application in debug mode")
		} else {
			logger = log.New()
		}
	},
}

func Command() *cobra.Command {
	return rootCmd
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func SetVersionInfo(version, commit, short string) {
	versionInfo.version = version
	versionInfo.commit = commit
	versionInfo.short = short

	rootCmd.Version = fmt.Sprintf("%s (Built from Git SHA %s)", versionInfo.version, versionInfo.commit)
}

func addRequiredDbFlag(cmd *cobra.Command) {
	cmd.PersistentFlags().StringVar(&databasePath, "db", "", "the path to the input/output database")
	cmd.MarkPersistentFlagRequired("db")
}

func addNoProgressFlag(cmd *cobra.Command) {
	cmd.Flags().BoolVar(&noProgress, "no-progress", false, "prevent displaying progress of long-running tasks")
}

func init() {
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "whether to enable debug logging")
}
