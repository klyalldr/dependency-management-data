package cmd

import (
	"github.com/spf13/cobra"
)

var ownersCmd = &cobra.Command{
	Use:   "owners",
	Short: "Manage the ownership of repositories",
}

func init() {
	rootCmd.AddCommand(ownersCmd)
	addRequiredDbFlag(ownersCmd)
}
