package cmd

import (
	"github.com/spf13/cobra"
)

var dbCmd = &cobra.Command{
	Use:   "db",
	Short: "Manage the database",
	Long:  `A set of commands to interact with the database.`,
}

func init() {
	rootCmd.AddCommand(dbCmd)
	addRequiredDbFlag(dbCmd)
}
