package cmd

import (
	"database/sql"
	"encoding/csv"
	"os"

	"dmd.tanna.dev/internal/ownership"
	"github.com/spf13/cobra"
)

var ownersImportCmd = &cobra.Command{
	Use:   "import repos.csv",
	Short: "Import the ownership of repo(s) from a CSV file",
	Long: `Import the ownership of repo(s) in bulk, by using a CSV file

NOTE that the CSV does not require a header

i.e.

dmd owners import repos.csv
`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		f, err := os.Open(args[0])
		cobra.CheckErr(err)

		defer f.Close()

		reader := csv.NewReader(f)
		reader.Comment = '#'

		records, err := reader.ReadAll()
		cobra.CheckErr(err)

		var rows []ownership.ImportOwnershipRow

		for _, r := range records {
			rows = append(rows, newImportOwnershipRow(r))
		}

		err = ownership.ImportOwnershipRows(cmd.Context(), rows, sqlDB)
		cobra.CheckErr(err)
	},
}

func init() {
	ownersCmd.AddCommand(ownersImportCmd)
}

func newImportOwnershipRow(r []string) ownership.ImportOwnershipRow {
	row := ownership.ImportOwnershipRow{
		Platform:     r[0],
		Organisation: r[1],
		Repo:         r[2],
		OwnerName:    r[3],
	}
	if len(r) == 5 {
		row.Notes = r[4]
	}

	return row
}
