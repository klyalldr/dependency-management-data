package main

import (
	"html/template"
	"net/http"

	"dmd.tanna.dev/internal/advisory"
)

type Report struct {
	Name        string
	Description string
}

func (s *DMDServer) handleReportRoot(w http.ResponseWriter, r *http.Request) {
	data := map[string]any{
		"Reports": s.reports,
	}

	s.renderTemplate(w, r, data, "templates/pages/report-root.html.tmpl")
}

func (s *DMDServer) registerReport(mux *http.ServeMux, reportRoute string, reportName string, reportDescription string, handlerFunc http.HandlerFunc) {
	if s.reports == nil {
		s.reports = make(map[string]Report)
	}

	s.reports["/report/"+reportRoute] = Report{
		Name:        reportName,
		Description: reportDescription,
	}
	mux.HandleFunc("/report/"+reportRoute, handlerFunc)
}

func (s *DMDServer) handleReportAdvisories(w http.ResponseWriter, r *http.Request) {
	report := `<h2>Package advisories</h2>`

	tw, err := advisory.ReportPackages(r.Context(), logger, s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	report += `<h2>AWS Infrastructure advisories</h2>`

	tw, err = advisory.ReportAWS(r.Context(), logger, s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}
