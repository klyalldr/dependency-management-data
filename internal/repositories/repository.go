package repositories

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/advisory"
	"dmd.tanna.dev/internal/datasources/awselasticache"
	"dmd.tanna.dev/internal/datasources/awslambda"
	"dmd.tanna.dev/internal/datasources/awsrds"
	"dmd.tanna.dev/internal/datasources/dependabot"
	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/datasources/sbom"
	"dmd.tanna.dev/internal/depsdev"
	"dmd.tanna.dev/internal/endoflifedate"
	"dmd.tanna.dev/internal/metadata"
	"dmd.tanna.dev/internal/osvdev"
	"dmd.tanna.dev/internal/ownership"
)

type Repository interface {
	CreateTables(ctx context.Context, sqlDB *sql.DB) error
	AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error)
}

var repositories = []Repository{
	&renovate.Renovate{},
	&dependabot.Dependabot{},
	&awselasticache.AWSElasticache{},
	&awslambda.AWSLambda{},
	&awsrds.AWSRds{},
	&endoflifedate.EndOfLifeDate{},
	&ownership.Ownership{},
	&osvdev.OsvDev{},
	&depsdev.DepsDev{},
	&advisory.Advisories{},
	&sbom.SBOMs{},
	&metadata.Metadata{},
}

func CreateTables(ctx context.Context, db *sql.DB) (err error) {
	for _, r := range repositories {
		err = r.CreateTables(ctx, db)
		if err != nil {
			return err
		}
	}

	return
}
