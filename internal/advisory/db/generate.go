package db

//go:generate go run dmd.tanna.dev/cmd/table-joiner schema.sql ../../datasources/renovate/db/schema.sql ../../datasources/sbom/db/schema.sql ../../endoflifedate/db/schema.sql ../../datasources/awslambda/db/schema.sql ../../datasources/awsrds/db/schema.sql ../../datasources/awselasticache/db/schema.sql
//go:generate go run github.com/kyleconroy/sqlc/cmd/sqlc generate
