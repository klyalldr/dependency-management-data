package db

import "context"

// HACK: https://github.com/kyleconroy/sqlc/issues/1546
func (q *Queries) ActuallyRetrievePackageAdvisories(ctx context.Context) ([]RetrievePackageAdvisoriesRow, error) {
	rows, err := q.db.QueryContext(ctx, retrievePackageAdvisories)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []RetrievePackageAdvisoriesRow
	for rows.Next() {
		var i RetrievePackageAdvisoriesRow
		if err := rows.Scan(
			&i.Platform,
			&i.Organisation,
			&i.Repo,
			&i.PackageName,
			&i.Version,
			&i.CurrentVersion,
			&i.DepTypes,
			&i.AdvisoryType,
			&i.Description,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

// HACK: https://github.com/kyleconroy/sqlc/issues/1546
func (q *Queries) ActuallyRetrieveAWSAdvisories(ctx context.Context) ([]RetrieveAWSAdvisoriesRow, error) {
	rows, err := q.db.QueryContext(ctx, retrieveAWSAdvisories)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []RetrieveAWSAdvisoriesRow
	for rows.Next() {
		var i RetrieveAWSAdvisoriesRow
		if err := rows.Scan(
			&i.Arn,
			&i.Name,
			&i.Runtime,
			&i.AdvisoryType,
			&i.Description,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
