-- name: RetrievePackageAdvisories :many
select
  s.platform,
  s.organisation,
  s.repo,
  s.package_name,
  s.version,
  s.current_version,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  a.advisory_type,
  a.description
from
  sboms s
  inner join advisories as a on s.package_name like replace(a.package_pattern, '*', '%')
where
s.version IS NOT NULL and
s.current_version IS NOT NULL and
(
   case
      when version_match_strategy IS NULL           then true
      when version_match_strategy = 'ANY'           then true
      when version_match_strategy = 'EQUAL'         then current_version = a.version
      when version_match_strategy = 'LESS_THAN'     then current_version < a.version
      when version_match_strategy = 'LESS_EQUAL'    then current_version <= a.version
      when version_match_strategy = 'GREATER_THAN'  then current_version > a.version
      when version_match_strategy = 'GREATER_EQUAL' then current_version >= a.version
      else false
    end)
union
select
  r.platform,
  r.organisation,
  r.repo,
  r.package_name,
  r.version,
  r.current_version,
  r.dep_types,
  advis.advisory_type,
  advis.description
from
  renovate r
  inner join advisories as advis on r.package_name like replace(advis.package_pattern, '*', '%')
where
(
   case
      when version_match_strategy IS NULL           then true
      when version_match_strategy = 'ANY'           then true
      when version_match_strategy = 'EQUAL'         then current_version = advis.version
      when version_match_strategy = 'LESS_THAN'     then current_version < advis.version
      when version_match_strategy = 'LESS_EQUAL'    then current_version <= advis.version
      when version_match_strategy = 'GREATER_THAN'  then current_version > advis.version
      when version_match_strategy = 'GREATER_EQUAL' then current_version >= advis.version
      else false
    end)
union
select
  renovate.platform,
  renovate.organisation,
  renovate.repo,
  e.package_name,
  e.version,
  e.current_version,
  renovate.dep_types,
  (
    case
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           < 0
          then
            'UNSUPPORTED'
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           < 0
          then
             'DEPRECATED'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  (
    case
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           < 0
          then
             e.product_name || ' ' || e.cycle
               || ' has been End-of-Life for '
               || abs(cast ((julianday(eol_from) - julianday('now')) as integer))
               || ' days'
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           <= 60
           then
             e.product_name || ' ' || e.cycle
               || ' will be End-of-Life in '
               || abs(cast ((julianday(eol_from) - julianday('now')) as integer))
               || ' days'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           < 0
          then
             e.product_name || ' ' || e.cycle
               || ' has been unsupported (usually only receiving critical security fixes) for '
               || abs(cast ((julianday(supported_until) - julianday('now')) as integer))
               || ' days'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           <= 60
           then
             e.product_name || ' ' || e.cycle
               || ' will be unsupported (usually only receiving critical security fixes) in '
               || abs(cast ((julianday(supported_until) - julianday('now')) as integer))
               || ' days'
    end
  ) as description
from
  renovate_endoflife e
  inner join endoflifedate_products on (
    e.product_name = endoflifedate_products.product_name
    AND e.cycle = endoflifedate_products.cycle
  )
  inner join renovate on (
    e.package_name = renovate.package_name
    AND (
      e.version = renovate.version
      AND case
        when e.current_version is NULL then true
        when e.current_version is NOT NULL then e.current_version = renovate.current_version
      end
    )
    AND e.package_manager = renovate.package_manager
    AND e.datasource = renovate.datasource
  )
where
  (supported_until is not null or eol_from is not null)
  AND
  (
    (
      cast ((julianday(supported_until) - julianday('now')) as integer)
      <= 60
    )
    OR
    (
      cast ((julianday(eol_from) - julianday('now')) as integer)
      <= 60
    )
  )
order by advisory_type, platform, organisation, repo, package_name;

-- name: AnonymiseAdvisoriesPackagePatternByOrg :exec
update advisories
set
  package_pattern  = 'ANON-' || hex(sha3(package_pattern))
where
    package_pattern like '%' || ? || '%'
;

-- name: RetrieveAWSAdvisories :many
select
  db.arn,
  db.name,
  db.engine || ' ' || db.engine_version as runtime,
  (
    case
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           < 0
          then
            'UNSUPPORTED'
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  (
    case
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           < 0
          then
             db.engine || ' ' || db.engine_version
               || ' has been End-of-Life for '
               || abs(cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
               || ' days'
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           <= 60
           then
             db.engine || ' ' || db.engine_version
               || ' will be End-of-Life in '
               || abs(cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
               || ' days'
    end
  ) as description
from
  aws_rds_databases as db
  natural join aws_rds_databases_engines
where
  (
    cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer)
    <= 100
  )
union
select
  f.arn,
  f.name,
  f.runtime as runtime,
  (
    case
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           < 0
          then
            'UNSUPPORTED'
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
      when r.deprecation is not null
      and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           < 0
          then
             'DEPRECATED'
      when r.deprecation is not null
      and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  (
    case
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           < 0
          then
            f.runtime
               || ' has been End-of-Life for '
              || abs(cast ((julianday(r.end_of_life) - julianday('now')) as integer))
               || ' days'
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           <= 60
           then
            f.runtime
               || ' will be End-of-Life in '
              || abs(cast ((julianday(r.end_of_life) - julianday('now')) as integer))
               || ' days'
      when r.deprecation is not null
      and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           < 0
          then
            f.runtime
               || ' has been deprecated '
               || abs(cast ((julianday(r.deprecation) - julianday('now')) as integer))
               || ' days'
      when r.deprecation is not null
        and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           <= 60
           then
            f.runtime
               || ' has been deprecated '
               || abs(cast ((julianday(r.deprecation) - julianday('now')) as integer))
               || ' days'
    end
  ) as description
from
  aws_lambda_functions f
  inner join aws_lambda_function_runtimes r on f.runtime = r.runtime
where
  (
    cast ((julianday(r.deprecation) - julianday('now')) as integer)
    <= 100
  )
union
select
  d.arn,
  d.name,
  d.engine || ' ' || d.engine_version as runtime,
  (
    case
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           < 0
          then
            'UNSUPPORTED'
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  (
    case
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           < 0
          then
             d.engine || ' ' || d.engine_version
               || ' has been End-of-Life for '
               || abs(cast ((julianday(e.deprecation) - julianday('now')) as integer))
               || ' days'
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           <= 60
           then
             d.engine || ' ' || d.engine_version
               || ' will be End-of-Life in '
               || abs(cast ((julianday(e.deprecation) - julianday('now')) as integer))
               || ' days'
    end
  ) as description
from
  aws_elasticache_datastores d
inner join aws_elasticache_datastore_engines e
where
      d.engine = e.engine
  and d.engine_version like e.engine_version || '.%'
  and
      (
        cast ((julianday(e.deprecation) - julianday('now')) as integer)
        <= 100
      )
;
