package advisory

import (
	"context"
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/advisory/db"
	"github.com/charmbracelet/log"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/mitchellh/go-wordwrap"
)

func ReportPackages(ctx context.Context, logger log.Logger, sqlDB *sql.DB) (table.Writer, error) {
	queries := db.New(sqlDB)

	rows, err := queries.ActuallyRetrievePackageAdvisories(ctx)
	if err != nil {
		return nil, err
	}

	tw := table.NewWriter()

	tw.AppendHeader(table.Row{
		"Platform",
		"Organisation",
		"Repo",
		"Package",
		"Version",
		"Dependency Types",
		"Advisory Type",
		"Description",
	})

	for _, row := range rows {
		if !row.Version.Valid {
			continue
		}
		ver := row.Version.String
		if row.CurrentVersion.Valid {
			ver = fmt.Sprintf("%s / %s", ver, row.CurrentVersion.String)
		}
		tw.AppendRow(table.Row{
			row.Platform,
			row.Organisation,
			row.Repo,
			row.PackageName,
			wordwrap.WrapString(ver, 10),
			row.DepTypes,
			row.AdvisoryType,
			wordwrap.WrapString(row.Description, 60),
		})
	}

	return tw, nil
}

func ReportAWS(ctx context.Context, logger log.Logger, sqlDB *sql.DB) (table.Writer, error) {
	queries := db.New(sqlDB)

	rows, err := queries.ActuallyRetrieveAWSAdvisories(ctx)
	if err != nil {
		return nil, err
	}

	tw := table.NewWriter()

	tw.AppendHeader(table.Row{
		"ARN",
		"Name",
		"Runtime",
		"Advisory Type",
		"Description",
	})

	for _, row := range rows {
		tw.AppendRow(table.Row{
			row.Arn,
			row.Name,
			row.Runtime,
			row.AdvisoryType,
			row.Description,
		})
	}

	return tw, err
}
