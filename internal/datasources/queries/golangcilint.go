package queries

type GolangCILintResult struct {
	Direct   []Repo
	Indirect []Repo
}
