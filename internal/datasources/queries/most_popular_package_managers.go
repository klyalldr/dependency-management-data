package queries

type MostPopularPackageManagersRow struct {
	PackageManager string
	Count          int64
}
