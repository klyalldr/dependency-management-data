package queries

type GorillaToolkitResult struct {
	Direct   []Count
	Indirect []Count
}
