package sbom

import (
	"dmd.tanna.dev/internal/datasources/sbom/cyclonedx"
	"dmd.tanna.dev/internal/datasources/sbom/spdx"
	"dmd.tanna.dev/internal/domain"
)

type SBOMFormat interface {
	Matches(body []byte) bool
	Name() string
	Parse(body []byte, platform string, org string, repo string) []domain.SBOMDependency
}

func Formats() []SBOMFormat {
	return []SBOMFormat{
		&spdx.SPDXv2_3Format{},
		&cyclonedx.CycloneDXv1_4Format{},
	}
}

func Identify(body []byte) (SBOMFormat, bool) {
	for _, format := range Formats() {
		if format.Matches(body) {
			return format, true
		}
	}

	return nil, false
}
