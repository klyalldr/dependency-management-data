-- sboms contains packages defined by Software Bill of Materials (SBOMs)
CREATE TABLE IF NOT EXISTS sboms (
  -- what platform hosts the source code that this SBOM was produced for? i.e. `github`, `gitlab`, `gitea`, etc
  platform TEXT NOT NULL,
  -- what organisation manages the source code that this SBOM was produced for? Can include `/` for nested organisations
  organisation TEXT NOT NULL,
  -- what repo manages the source code that this SBOM was produced for?
  repo TEXT NOT NULL,

  package_name TEXT NOT NULL,
  version TEXT,
  current_version TEXT,

  -- package_type most commonly relates to the "Type" field of a Package URL, which may be a package ecosystem or package manager type
  package_type TEXT NOT NULL,

  UNIQUE (platform, organisation, repo, package_name, package_type) ON CONFLICT REPLACE
);
