package cyclonedx

import (
	"bytes"
	"encoding/json"

	"dmd.tanna.dev/internal/domain"
	cdx "github.com/CycloneDX/cyclonedx-go"
	"github.com/package-url/packageurl-go"
)

type CycloneDXv1_4Format struct{}

type cycloneDXv1_4Body struct {
	BOMFormat   string `json:"bomFormat"`
	SpecVersion string `json:"specVersion"`
}

func (s *CycloneDXv1_4Format) Matches(body []byte) bool {
	var b cycloneDXv1_4Body
	err := json.Unmarshal(body, &b)
	if err != nil {
		return false
	}

	if b.BOMFormat != "CycloneDX" || b.SpecVersion != "1.4" {
		return false
	}

	var bom cdx.BOM

	decoder := cdx.NewBOMDecoder(bytes.NewReader(body), cdx.BOMFileFormatJSON)
	err = decoder.Decode(&bom)
	if err != nil {
		return false
	}

	if bom.SpecVersion != cdx.SpecVersion1_4 {
		return false
	}
	return true
}

func (s *CycloneDXv1_4Format) Name() string {
	return "CycloneDX-1.4"
}

func (s *CycloneDXv1_4Format) Parse(body []byte, platform string, org string, repo string) []domain.SBOMDependency {
	var bom cdx.BOM

	decoder := cdx.NewBOMDecoder(bytes.NewReader(body), cdx.BOMFileFormatJSON)
	err := decoder.Decode(&bom)
	if err != nil {
		return nil
	}

	return s.parseInternal(&bom, platform, org, repo)
}

func (s *CycloneDXv1_4Format) parseInternal(bom *cdx.BOM, platform string, org string, repo string) []domain.SBOMDependency {
	if bom.Components == nil {
		return nil

	}
	var deps []domain.SBOMDependency

	for _, c := range *bom.Components {
		purl, err := packageurl.FromString(c.PackageURL)
		if err != nil {
			continue
		}

		d := domain.SBOMDependency{
			Platform:       platform,
			Organisation:   org,
			Repo:           repo,
			PackageName:    c.Name,
			Version:        toNilIfEmpty(c.Version),
			CurrentVersion: currentVersion(c.Version),
			PackageType:    purl.Type,
		}

		deps = append(deps, d)
	}

	return deps
}
