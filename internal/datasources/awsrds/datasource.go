package awsrds

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awsrds/db"
)

type AWSRds struct{}

func (*AWSRds) Name() string {
	return "AWS RDS"
}

func (*AWSRds) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
