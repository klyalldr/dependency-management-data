-- name: InsertDatastore :exec
INSERT INTO aws_elasticache_datastores (
  account_id,
  region,
  arn,
  name,
  engine,
  engine_version,
  tags
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveAll :many
select * from aws_elasticache_datastores;

-- name: AnonymiseElasticacheDatastores :exec
update aws_elasticache_datastore_engines
set
  account_id  = 'ANON-' || hex(sha3(account_id)),
  arn  = 'ANON-' || hex(sha3(arn)),
  name  = 'ANON-' || hex(sha3(name)),
  tags          = 'ANON-' || hex(sha3(tags))
where
  account_id
    NOT LIKE 'ANON-%'
  OR
  arn
    NOT LIKE 'ANON-%'
  OR
  name
    NOT LIKE 'ANON-%'
  OR
  tags
    NOT LIKE 'ANON-%'
;

-- name: InsertDatastoreEngine :exec
INSERT INTO aws_elasticache_datastore_engines (
  engine,
  engine_version,
  deprecation
) VALUES (
  ?,
  ?,
  ?
);

-- name: RetrieveAllWithDeprecation :many
select
  arn,
  name,
  d.engine,
  d.engine_version,
  e.deprecation
from
  aws_elasticache_datastores d
inner join aws_elasticache_datastore_engines e
where d.engine = e.engine
and d.engine_version like e.engine_version || '.%'
