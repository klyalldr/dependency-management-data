package awselasticache

import (
	"encoding/json"
	"log"

	"dmd.tanna.dev/internal/datasources/awselasticache/db"
)

type Datastore struct {
	AccountId     string
	Region        string
	ARN           string
	Name          string
	Engine        string
	EngineVersion string
	Tags          map[string]string
}

func newDatastoreFromDB(dbRow db.AwsElasticacheDatastore) Datastore {
	f := Datastore{
		AccountId:     dbRow.AccountID,
		Region:        dbRow.Region,
		ARN:           dbRow.Arn,
		Name:          dbRow.Name,
		Engine:        dbRow.Engine,
		EngineVersion: dbRow.EngineVersion,
	}

	err := json.Unmarshal([]byte(dbRow.Tags), &f.Tags)
	if err != nil {
		log.Printf("Failed to unmarshal `Tags` from DB row for RDS Database ARN %s", f.ARN)
	}

	return f
}
