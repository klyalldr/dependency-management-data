package renovate

import (
	"encoding/json"

	"dmd.tanna.dev/internal/datasources/renovate/db"
	"dmd.tanna.dev/internal/domain"
)

type Dependency struct {
	domain.Dependency

	Datasource string
	DepTypes   []string
}

type DependencyUpdate struct {
	domain.DependencyUpdate

	Datasource string
}

func newDependencyFromDB(dbRow db.Renovate) Dependency {
	dep := Dependency{
		Dependency: domain.Dependency{
			Platform:        dbRow.Platform,
			Organisation:    dbRow.Organisation,
			Repo:            dbRow.Repo,
			PackageName:     dbRow.PackageName,
			Version:         dbRow.Version,
			PackageManager:  dbRow.PackageManager,
			PackageFilePath: dbRow.PackageFilePath,
		},
		Datasource: dbRow.Datasource,
		DepTypes:   []string{},
	}

	if dbRow.CurrentVersion.Valid {
		dep.CurrentVersion = &dbRow.CurrentVersion.String
	}

	var depTypes []string
	err := json.Unmarshal([]byte(dbRow.DepTypes), &depTypes)

	if err != nil {
		dep.DepTypes = depTypes
	}

	return dep
}
