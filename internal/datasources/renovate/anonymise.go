package renovate

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/renovate/db"
)

func (*Renovate) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	renDB := db.New(sqlDB)
	err = renDB.AnonymiseOrgAndRepo(ctx)
	if err != nil {
		return
	}

	err = renDB.AnonymiseUpdatesOrgAndRepo(ctx)
	if err != nil {
		return
	}

	for _, org := range orgs {
		err = renDB.AnonymisePackageNameByOrg(ctx, org)
		if err != nil {
			return
		}

		err = renDB.AnonymisePackageFilePathByOrg(ctx, org)
		if err != nil {
			return
		}

		err = renDB.AnonymiseVersionByOrg(ctx, org)
		if err != nil {
			return
		}

		err = renDB.AnonymiseUpdatesPackageNameByOrg(ctx, org)
		if err != nil {
			return
		}

		err = renDB.AnonymiseUpdatesPackageFilePathByOrg(ctx, org)
		if err != nil {
			return
		}

		err = renDB.AnonymiseUpdatesVersionByOrg(ctx, org)
		if err != nil {
			return
		}
	}

	return
}
