package awslambda

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/jedib0t/go-pretty/v6/progress"
	"gitlab.com/tanna.dev/endoflife-checker/awslambda"
)

type parser struct{}

func NewParser() parser {
	return parser{}
}

func (p parser) ParseFile(filename string) (f LambdaFunction, _ error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return f, err
	}

	var report awslambda.Report
	err = json.Unmarshal(data, &report)
	if err != nil {
		return f, err
	}

	f = LambdaFunction{
		AccountId:    report.Function.AccountId,
		Region:       report.Function.Region,
		ARN:          report.Function.ARN,
		Name:         report.Function.Name,
		Runtime:      report.Function.Runtime,
		LastModified: report.Function.LastModified,
		Tags:         report.Function.Tags,
	}

	return f, nil
}

func (p parser) ParseFiles(glob string, pw progress.Writer) ([]LambdaFunction, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, err
	}

	if files == nil {
		return nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	tracker := progress.Tracker{
		Message: "Parsing aws-lambda-endoflife files",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	var wg sync.WaitGroup
	functions := make([]LambdaFunction, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, filename string) {
			defer wg.Done()
			defer tracker.Increment(1)

			f, err := p.ParseFile(filename)
			if err != nil {
				log.Printf("Failed to parse %s: %v", filename, err)
				return
			}
			functions[i] = f
		}(i, f)
	}

	wg.Wait()
	tracker.UpdateMessage(fmt.Sprintf("Parsed %d aws-lambda-endoflife files", tracker.Total))
	tracker.MarkAsDone()

	return functions, nil
}
