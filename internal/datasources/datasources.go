package datasources

import (
	"dmd.tanna.dev/internal/datasources/awselasticache"
	"dmd.tanna.dev/internal/datasources/awslambda"
	"dmd.tanna.dev/internal/datasources/awsrds"
	"dmd.tanna.dev/internal/datasources/dependabot"
	"dmd.tanna.dev/internal/datasources/renovate"
)

type Datasource interface{}

var datasources = []Datasource{
	&renovate.Renovate{},
	&dependabot.Dependabot{},
	&awselasticache.AWSElasticache{},
	&awslambda.AWSLambda{},
	&awsrds.AWSRds{},
}
