package datasources

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/queries"
)

type querier interface {
	Name() string
}

type queryMostPopularPackageManagers interface {
	querier
	QueryMostPopularPackageManagers(ctx context.Context, sqlDB *sql.DB) ([]queries.MostPopularPackageManagersRow, error)
}

func QueryMostPopularPackageManagers(ctx context.Context, db *sql.DB) (map[string][]queries.MostPopularPackageManagersRow, error) {
	all := make(map[string][]queries.MostPopularPackageManagersRow)

	for _, d := range datasources {
		q, ok := d.(queryMostPopularPackageManagers)
		if ok {
			result, err := q.QueryMostPopularPackageManagers(ctx, db)
			if err != nil {
				return nil, err
			}
			all[q.Name()] = result
		}
	}

	return all, nil
}

type queryMostPopularDockerImages interface {
	querier
	QueryMostPopularDockerImages(ctx context.Context, sqlDB *sql.DB) (result queries.MostPopularDockerImagesResult, err error)
}

func QueryMostPopularDockerImages(ctx context.Context, db *sql.DB) (map[string]queries.MostPopularDockerImagesResult, error) {
	all := make(map[string]queries.MostPopularDockerImagesResult)

	for _, d := range datasources {
		q, ok := d.(queryMostPopularDockerImages)
		if ok {
			result, err := q.QueryMostPopularDockerImages(ctx, db)
			if err != nil {
				return nil, err
			}
			all[q.Name()] = result
		}
	}

	return all, nil
}

type queryGorillaToolkit interface {
	querier
	QueryGorillaToolkit(ctx context.Context, sqlDB *sql.DB) (result queries.GorillaToolkitResult, err error)
}

func QueryGorillaToolkit(ctx context.Context, db *sql.DB) (map[string]queries.GorillaToolkitResult, error) {
	all := make(map[string]queries.GorillaToolkitResult)

	for _, d := range datasources {
		q, ok := d.(queryGorillaToolkit)
		if ok {
			result, err := q.QueryGorillaToolkit(ctx, db)
			if err != nil {
				return nil, err
			}
			all[q.Name()] = result
		}
	}

	return all, nil
}

type queryGolangCILint interface {
	querier
	QueryGolangCILint(ctx context.Context, sqlDB *sql.DB) (result queries.GolangCILintResult, err error)
}

func QueryGolangCILint(ctx context.Context, db *sql.DB) (map[string]queries.GolangCILintResult, error) {
	all := make(map[string]queries.GolangCILintResult)

	for _, d := range datasources {
		q, ok := d.(queryGolangCILint)
		if ok {
			result, err := q.QueryGolangCILint(ctx, db)
			if err != nil {
				return nil, err
			}
			all[q.Name()] = result
		}
	}

	return all, nil
}

type queryDistinctProjects interface {
	querier
	QueryDistinctProjects(ctx context.Context, db *sql.DB, params queries.DistinctProjectParams) ([]queries.DistinctProject, error)
}

func QueryDistinctProjects(ctx context.Context, db *sql.DB, params queries.DistinctProjectParams) (map[string][]queries.DistinctProject, error) {
	all := make(map[string][]queries.DistinctProject)

	for _, d := range datasources {
		q, ok := d.(queryDistinctProjects)
		if ok {
			result, err := q.QueryDistinctProjects(ctx, db, params)
			if err != nil {
				return nil, err
			}
			all[q.Name()] = result
		}
	}

	return all, nil
}
