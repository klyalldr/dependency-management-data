package osvdev

import (
	"context"
	"database/sql"
	"fmt"

	renovatedb "dmd.tanna.dev/internal/datasources/renovate/db"
	"dmd.tanna.dev/internal/osvdev/db"
)

type OsvDev struct{}

func (*OsvDev) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}

func (*OsvDev) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	queries := db.New(sqlDB)

	for _, org := range orgs {
		err = queries.AnonymisePackageNameByOrg(ctx, org)
		if err != nil {
			return
		}
	}

	return
}

type Package struct {
	Name      string `json:"name"`
	Ecosystem string `json:"ecosystem"`
}

type Query struct {
	Version string  `json:"version"`
	Package Package `json:"package"`
}

func NewRenovateQuery(dep renovatedb.RetrieveDistinctPackagesRow) (q Query, ok bool) {
	q.Version = dep.Version
	q.Package.Name = dep.PackageName

	switch dep.Datasource {
	case "go":
		q.Package.Ecosystem = "Go"
		ok = true
	case "npm":
		q.Package.Ecosystem = "npm"
		ok = true
	case "pypi":
		q.Package.Ecosystem = "PyPI"
		ok = true
	case "rubygems":
		q.Package.Ecosystem = "RubyGems"
		ok = true
	case "crate":
		q.Package.Ecosystem = "crates.io"
		ok = true
	case "packagist":
		q.Package.Ecosystem = "Packagist"
		ok = true
	case "maven":
		q.Package.Ecosystem = "Maven"
		ok = true
	case "nuget":
		q.Package.Ecosystem = "NuGet"
		ok = true
	}

	if dep.PackageManager == "github-actions" {
		q.Package.Ecosystem = "GitHub Actions"
		ok = true
	}

	return
}

type Vuln struct {
	ID string `json:"id"`
}

func (v Vuln) URL() string {
	return fmt.Sprintf("https://osv.dev/vulnerability/%s", v.ID)
}

type Vulns struct {
	Query Query
	Vulns []Vuln
}
