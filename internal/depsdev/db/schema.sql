CREATE TABLE IF NOT EXISTS depsdev_cves (
  package_name TEXT NOT NULL,
  version TEXT NOT NULL,

  cve_id TEXT NOT NULL,

  updated_at TEXT NOT NULL,

  UNIQUE (package_name, version, cve_id) ON CONFLICT REPLACE
);

CREATE TABLE IF NOT EXISTS depsdev_licenses (
  package_name TEXT NOT NULL,
  version TEXT NOT NULL,

  license TEXT NOT NULL,

  updated_at TEXT NOT NULL,

  UNIQUE (package_name, version, license) ON CONFLICT REPLACE
);
