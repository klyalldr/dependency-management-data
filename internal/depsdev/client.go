package depsdev

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"dmd.tanna.dev/internal/datasources/renovate/db"
)

const baseURL = "https://api.deps.dev/v3alpha"

type PackageVersionQuery struct {
	System  string
	Name    string
	Version string
}

func newRenovatePackageVersionQuery(dep db.RetrieveDistinctPackagesRow) (q PackageVersionQuery, ok bool) {
	q.Name = dep.PackageName

	q.Version = dep.Version
	if dep.CurrentVersion.Valid {
		q.Version = dep.CurrentVersion.String
	}

	if q.Version == "" {
		return
	}

	switch dep.Datasource {
	case "go":
		q.System = "GO"
		ok = true
	case "npm":
		q.System = "NPM"
		ok = true
	case "cargo":
		q.System = "CARGO"
		ok = true
	case "maven":
		q.System = "MAVEN"
		ok = true
	case "pypi":
		q.System = "PYPI"
		ok = true
	}

	return
}

type Client struct {
	httpClient *http.Client
}

func NewClient(httpClient *http.Client) Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	return Client{
		httpClient: httpClient,
	}
}

type GetVersionResponse struct {
	VersionKey struct {
		System  string `json:"system"`
		Name    string `json:"name"`
		Version string `json:"version"`
	} `json:"versionKey"`
	Licenses     []string `json:"licenses"`
	AdvisoryKeys []struct {
		ID string `json:"id"`
	} `json:"advisoryKeys"`
}

func (c *Client) GetVersion(q PackageVersionQuery) (resp GetVersionResponse, err error, notFound bool) {
	u := fmt.Sprintf("%s/systems/%s/packages/%s/versions/%s", baseURL, q.System, url.PathEscape(q.Name), url.PathEscape(q.Version))

	httpResp, err := c.httpClient.Get(u)
	if err != nil {
		return
	}
	defer httpResp.Body.Close()

	if httpResp.StatusCode != 200 {
		notFound = httpResp.StatusCode == http.StatusNotFound

		return resp, fmt.Errorf("received an HTTP %d response query GetVersion %v", httpResp.StatusCode, q), notFound
	}

	body, err := io.ReadAll(httpResp.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &resp)
	if err != nil {
		return
	}

	return
}
