package domain

type Owner struct {
	Platform     string
	Organisation string
	Repo         string

	Name  string
	Notes string
}
