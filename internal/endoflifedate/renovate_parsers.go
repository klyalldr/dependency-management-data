package endoflifedate

import (
	"dmd.tanna.dev/internal/endoflifedate/db"
	"github.com/charmbracelet/log"
)

var renovateParsers = []renovateParser{}

type renovateParser interface {
	Handled(dep db.RetrieveDistinctRenovateDepsRow) bool
	ParseProductAndCycle(logger log.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool)
}
