package endoflifedate

import "database/sql"

func getDepVersion(version string, currentVersion sql.NullString) string {
	if currentVersion.Valid {
		return currentVersion.String
	}
	return version
}
