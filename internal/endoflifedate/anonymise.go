package endoflifedate

import (
	"context"
	"database/sql"
	"strings"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

func (*EndOfLifeDate) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	db := db.New(sqlDB)

	err = db.AnonymiseRenovateOrgAndRepo(ctx)
	if err != nil {
		// in this case, we've not yet generated the tables, so this error is expected, but we should find a better way to do this
		if strings.Contains(err.Error(), "no such table") {
			return nil
		}

		return
	}

	for _, org := range orgs {
		err = db.AnonymiseRenovatePackageFilePathByOrg(ctx, org)
		if err != nil {
			return
		}

	}

	err = db.AnonymiseDependabotOrgAndRepo(ctx)
	if err != nil {
		return
	}

	return nil
}
