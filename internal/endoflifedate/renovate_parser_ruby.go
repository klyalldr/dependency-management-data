package endoflifedate

import (
	"fmt"
	"regexp"
	"strings"

	"dmd.tanna.dev/internal/endoflifedate/db"
	"github.com/charmbracelet/log"
)

func init() {
	renovateParsers = append(renovateParsers, &renovateParserRuby{})
}

type renovateParserRuby struct{}

func (*renovateParserRuby) Handled(dep db.RetrieveDistinctRenovateDepsRow) bool {
	return dep.PackageName == "ruby" && dep.PackageManager == "ruby-version"
}

func (*renovateParserRuby) ParseProductAndCycle(logger log.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool) {
	p = "ruby"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(getDepVersion(dep.Version, dep.CurrentVersion), "")

	match, _ := regexp.MatchString("^[0-9]+$", version)
	if match {
		logger.Warn(fmt.Sprintf("Couldn't assume cycle of %s that %s corresponds to", p, dep.DependencyDetails()))
		return
	}

	match, _ = regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}

	match, _ = regexp.MatchString("^ruby-[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(strings.ReplaceAll(version, "ruby-", ""))

		return p, cycle, true
	}

	return
}
