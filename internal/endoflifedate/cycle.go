package endoflifedate

import "time"

type Cycle struct {
	// Cycle Release Cycle, i.e. `19`, `22.04`
	Cycle string
	// SupportedUntil Date supported until
	SupportedUntil string
	// EolFrom End of Life Date for this release cycle
	EolFrom string
}

// IsUnsupported whether the cycle is still actively supported
func (c Cycle) IsUnsupported() bool {
	if c.SupportedUntil != "" {
		supportedDate, err := time.Parse("2006-01-02", c.SupportedUntil)
		if err != nil {
			return false
		}
		now := time.Now()
		return now.After(supportedDate)
	}

	return c.IsEol()
}

// IsEol Whether the cycle is End Of Life (EOL)
func (c Cycle) IsEol() bool {
	eolDate, err := time.Parse("2006-01-02", c.EolFrom)
	if err != nil {
		return false
	}
	now := time.Now()
	return now.After(eolDate)
}
