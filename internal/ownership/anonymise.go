package ownership

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/ownership/db"
)

func (*Ownership) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	q := db.New(sqlDB)

	err = q.AnonymiseOrgAndRepo(ctx)
	return
}
