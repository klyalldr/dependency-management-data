package ownership

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/queries"
	"dmd.tanna.dev/internal/datasources/renovate"
)

var projectQueriers = []DistinctProjectsQuerier{
	&renovate.Renovate{},
}

type DistinctProjectsQuerier interface {
	QueryDistinctProjects(ctx context.Context, db *sql.DB, params queries.DistinctProjectParams) ([]queries.DistinctProject, error)
}
