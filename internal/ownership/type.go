package ownership

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/ownership/db"
)

type Ownership struct{}

func (*Ownership) CreateTables(ctx context.Context, sqlDB *sql.DB) (err error) {
	_, err = sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return
}
