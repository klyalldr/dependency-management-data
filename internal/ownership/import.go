package ownership

import (
	"context"
	"database/sql"
	"time"

	"dmd.tanna.dev/internal/ownership/db"
)

type ImportOwnershipRow struct {
	Platform     string
	Organisation string
	Repo         string

	OwnerName string
	Notes     string
}

func ImportOwnershipRows(ctx context.Context, rows []ImportOwnershipRow, sqlDB *sql.DB) error {
	queries := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, row := range rows {
		p := db.InsertOwnerParams{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
			Owner:        row.OwnerName,
			UpdatedAt:    time.Now().Format(time.RFC3339),
		}

		if row.Notes != "" {
			p.Notes = sql.NullString{
				String: row.Notes,
				Valid:  true,
			}
		}

		err := queries.WithTx(tx).InsertOwner(ctx, p)
		if err != nil {
			return err
		}
	}

	return tx.Commit()
}
