---
title: Related
---
Dependency-management-data relies on various tools for producing the datasources that are used by the `dmd` CLI:

- [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph) for using [Renovate](https://docs.renovatebot.com)'s excellent support for package ecosystems to extract dependency data
- [dependabot-graph](https://gitlab.com/tanna.dev/dependabot-graph) for using GitHub Advanced Security's Dependabot dependency graph functionality to extract dependency data
- [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker) for various types of infrastructure lookups, such as AWS Lambda and RDS

Other related discussions about dependency-management-data:

- [Quantifying your reliance on Open Source software](https://www.jvt.me/posts/2023/07/25/dmd-talk/)
- [Getting started with Dependency Management](https://www.jvt.me/posts/2023/07/25/dmd-getting-started/)
- [Custom Advisories: the unsung hero of dependency-management-data](https://www.jvt.me/posts/2023/08/29/dmd-custom-advisories/)
- [Working out which Docker namespaces and images you most depend on](https://www.jvt.me/posts/2023/03/15/dmd-docker-usage/)
