---
title: Features
---

<table>
<tr>
<th>
Feature
</th>
<th>
Renovate
</th>
<th>
Software Bill of Materials (SBOM)
</th>
<th>
AWS Infrastructure
</th>
</tr>
<tr>
</tr>

<tr>
<td>
    Advisories
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    Database anonymisation
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>

<tr>
<td>
    End-of-Life lookups, via <a href="https://endoflife.date">EndOfLife.date</a>
</td>
<td>
✅
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    CVE Lookups, via <a href="https://osv.dev">osv.dev</a>
</td>
<td>
✅
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Licensing + CVE Lookups, via <a href="https://deps.dev">deps.dev</a>
</td>
<td>
✅
</td>
<td>
</td>
<td>
</td>
</tr>

</table>

## Reports

<table>
<tr>
<th>
Report
</th>
<th>
Renovate
</th>
<th>
Software Bill of Materials (SBOM)
</th>
</tr>
<tr>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_advisories">advisories</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_golangCILint">golangCILint</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_mostPopularDockerImages">mostPopularDockerImages</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_mostPopularPackageManagers">mostPopularPackageManagers</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

</table>

## End-of-Life checking

Via [endoflife.date](https://endoflife.date)

<table>
<tr>
<th>
Product
</th>
<th>
Renovate
</th>
<th>
Software Bill of Materials (SBOM)
</th>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/go>Go</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/alpine>Alpine</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/nodejs>NodeJS</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/python>Python</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/redis>Redis</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/ruby>Ruby</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

</table>
